/**
 * @file
 * Javascript lib.
 */

(function ($) {
    $.fn
        .extend({
            socialshare: function (options) {
                var settings = $.extend({
                    speed: 0,
                    animate: false,
                    upperLimitElementId: 'main',
                    lowerLimitElementId: 'footer',
                    leftLimitElementId: 'main',
                    marginTop: 0,
                    target: '#socialshare-wrapper'
                }, options);
                return this
                    .each(function () {

                        var PosTop, PosLeft, PosBottom;
                        if (settings.upperLimitElementId) {
                            PosTop = $(
                                '#' + settings.upperLimitElementId)
                                .offset().top;
                        } else {
                            PosTop = 0;
                        }

                        if (settings.leftLimitElementId) {
                            PosLeft = $(
                                '#' + settings.leftLimitElementId)
                                .offset().left;
                        } else {
                            PosLeft = 0;
                        }

                        if (settings.lowerLimitElementId
                            && $('#' + settings.lowerLimitElementId).length === 1) {
                            PosBottom = $(
                                '#' + settings.lowerLimitElementId)
                                .offset().top;
                        } else {
                            PosBottom = 0;
                        }

                        var socialBox = {
                            top: PosTop,
                            left: PosLeft,
                            bottom: PosBottom
                        };

                        var widget = $(this).css({
                            top: socialBox.top,
                            left: socialBox.left - widgetWidth()
                        });

                        $(window).scroll(scrollWidget).resize(
                            scrollWidget);

                        // widget width.
                        function widgetWidth() {
                            var widgetWidth = $(settings.target)
                                .width();
                            return widgetWidth;
                        }

                        // widget left postion
                        function widgetPosLeft() {
                            var updatePosLeft = $(
                                '#' + settings.leftLimitElementId)
                                .offset().left;
                            return updatePosLeft - widgetWidth();
                        }

                        function scrollWidget() {
                            var scrollTop = $(window).scrollTop();
                            var top = scrollTop > socialBox.top ? scrollTop
                                : socialBox.top;
                            var left = widgetPosLeft();

                            var bottom = PosBottom === 0 ? 0 : socialBox.bottom - widget.outerHeight();

                            if (bottom != 0) {
                                if (top < bottom) {
                                    if (top <= socialBox.top) {
                                        widget.stop(true, false)
                                            .animate({
                                                top: top,
                                                left: left
                                            }, settings.speed);
                                    } else {
                                        widget
                                            .stop(true, false)
                                            .animate(
                                            {
                                                top: top + parseInt(settings.marginTop),
                                                left: left
                                            },
                                            settings.speed);
                                    }
                                } else
                                    widget.stop(true, false).animate({
                                        top: bottom,
                                        left: left
                                    }, settings.speed);
                            } else {
                                // Default behaviour with bottom
                                // tracking.
                                if (top > socialBox.top) {
                                    widget
                                        .stop(true, false)
                                        .animate(
                                        {
                                            top: top + parseInt(settings.marginTop),
                                            left: left
                                        }, settings.speed);
                                } else {
                                    widget.stop(true, false).animate({
                                        top: top,
                                        left: left
                                    }, settings.speed);
                                }
                            }

                        }

                        return this;
                    });
            }
        });
})(jQuery);

(function ($, Drupal, window, document, undefined) {

    Drupal.behaviors.socialshare = {
        attach: function (context, settings) {
            setTimeout(function () {
                $('div.socialshare-box').show();
                var marginTop = Drupal.settings['socialshare']['marginTop'];
                $('#socialshare-wrapper').socialshare({
                    marginTop: marginTop
                });
            }, 100);
        }
    }

})(jQuery, Drupal, this, this.document);
