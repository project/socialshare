<?php

/**
 * @file
 * Template display social sharing buttons.
 */

?>

<div id="socialshare-wrapper">
  <div class="socialshare-box"<?php print $widget_style ?>>
    <ul class="socialshare-icon">
      <?php
      foreach ($buttons as $button):
        print '<li>' . _socialshare_display_button($button->button_icon, $button->button_name, $button->button_codes) . '</li>';
      endforeach;
      ?>
    </ul>
  </div>
</div>
