<?php

/**
 * @file
 * module admin.
 */

// Load form helper.
module_load_include('inc', 'socialshare', 'includes/socialshare.admin.form');

/**
 * Form builder: Configure the socialshare system.
 */
function _socialshare_settings() {
  $form = array();
  $form = array_merge($form, _socialshare_subform_socialshare_display());
  $form = array_merge($form, _socialshare_subform_socialshare_customize());
  return system_settings_form($form);
}

/**
 * Form builder: Buttons management.
 */
function _socialshare_button_management() {
  $form = _socialshare_subform_socialshare_buttons();
  return system_settings_form($form);
}

/**
 * Provide a single block from the administration menu as a page.
 */
function _socialshare_settings_landing() {
  $item = menu_get_item();
  $content = system_admin_menu_block($item);
  if ($content) {
    $output = theme('admin_block_content', array(
      'content' => $content,
    ));
  }
  else {
    $output = t('You do not have any administrative items.');
  }
  return $output;
}
