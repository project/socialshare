<?php

/**
 * @file
 * Admin forms.
 */

/**
 * Sub Form builder: Share buttons.
 */
function _socialshare_subform_socialshare_buttons() {
  $form['socialshare_buttons'] = array(
    '#type' => 'fieldset',
    '#title' => t('Share Buttons'),
    '#description' => t('Social share buttons list'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['socialshare_buttons']['list'] = array(
    '#theme' => 'socialshare_admin_buttons_list',
    '#weight' => 0,
  );
  return $form;
}

/**
 * Sub Form builder: Share buttons.
 */
function _socialshare_subform_socialshare_customize() {
  // Customize.
  $form['socialshare_customize'] = array(
    '#type' => 'fieldset',
    '#weight' => 3,
    '#title' => t('Customize'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['socialshare_customize']['socialshare_customize_bgcolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color'),
    '#description' => t('for example: #FFFFFF'),
    '#size' => 10,
    '#default_value' => variable_get('socialshare_customize_bgcolor', ''),
  );
  $form['socialshare_customize']['socialshare_customize_position_margintop'] = array(
    '#type' => 'textfield',
    '#title' => t('Scroll postion top'),
    '#description' => t('number only. for example: 60'),
    '#size' => 10,
    '#default_value' => variable_get('socialshare_customize_position_margintop', 60),
  );
  return $form;
}

/**
 * Sub Form builder: Display.
 */
function _socialshare_subform_socialshare_display() {
  // Socialshare display rules.
  $form['socialshare_display'] = array(
    '#type' => 'fieldset',
    '#weight' => 1,
    '#title' => t('Display conditions'),
    '#description' => t('Add socialshare widget to the following content types.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['socialshare_display']['socialshare_display_pages_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Automatically add to all pages except content pages.'),
    '#default_value' => variable_get('socialshare_display_pages_enabled', TRUE),
  );
  $form['socialshare_display']['nodetypes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $node_types = node_type_get_types();
  $node_names = node_type_get_names();
  if (is_array($node_names) && count($node_names)) {
    foreach ($node_names as $key => $value) {
      $form['socialshare_display']['nodetypes']['socialshare_display_' . $node_types[$key]->type . '_enabled'] = array(
        '#type' => 'checkbox',
        '#title' => t('Automatically add socialshare to content type @value (only affects content type @value)', array(
          '@value' => $value,
        )),
        '#default_value' => variable_get('socialshare_display_' . $node_types[$key]->type . '_enabled', TRUE),
      );
    }
  }
  return $form;
}

/**
 * Form builder: create buttons.
 */
function _socialshare_button_add($form, &$form_state, $edit = array()) {
  $button = new stdClass();
  $button->button_name = '';
  $button->button_machine_name = '';
  $button->button_codes = '';
  $button->button_icon = '';
  $button->button_enabled = '';
  $button->button_weight = '';
  $form['button_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $button->button_name,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['button_machine_name'] = array(
    '#type' => 'machine_name',
    '#default_value' => $button->button_machine_name,
    '#maxlength' => 21,
    '#machine_name' => array(
      'exists' => 'socialshare_button_machine_name_load',
      'source' => array('button_name'),
    ),
  );
  $form['button_codes'] = array(
    '#title' => t('Button share codes'),
    '#type' => 'textarea',
    '#description' => t('Share codes such as url or javascript codes'),
    '#wysiwyg' => FALSE,
    '#required' => TRUE,
    '#default_value' => $button->button_codes,
  );
  $form['button_icon'] = array(
    '#title' => t('Upload button/icon'),
    '#type' => 'managed_file',
    '#description' => t('Allowed extensions: gif png jpg jpeg'),
    '#default_value' => $button->button_icon,
    '#required' => FALSE,
    '#upload_validators' => array(
      'file_validate_extensions' => array(
        'gif png jpg jpeg',
      ),
      // Pass the maximum file size in bytes.
      'file_validate_size' => array(1 * 1024 * 1024),
    ),
    '#upload_location' => 'public://socialshare/icons/',
  );
  $form['button_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => $button->button_enabled,
  );

  $form['button_weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#delta' => 50,
    '#default_value' => $button->button_weight,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Submit handler for adding button.
 */
function _socialshare_button_add_submit($form, &$form_state) {
  $buttons_serialize = variable_get('socialshare_buttons', _socialshare_buttons_default());
  $buttons = unserialize($buttons_serialize);

  $buttons[$form_state['values']['button_machine_name']] = new stdClass();
  $buttons[$form_state['values']['button_machine_name']]->button_machine_name = $form_state['values']['button_machine_name'];
  $buttons[$form_state['values']['button_machine_name']]->button_name = $form_state['values']['button_name'];
  $buttons[$form_state['values']['button_machine_name']]->button_codes = $form_state['values']['button_codes'];
  $buttons[$form_state['values']['button_machine_name']]->button_icon = $form_state['values']['button_icon'];
  $buttons[$form_state['values']['button_machine_name']]->button_enabled = $form_state['values']['button_enabled'];
  $buttons[$form_state['values']['button_machine_name']]->button_weight = $form_state['values']['button_weight'];

  variable_set('socialshare_buttons', serialize($buttons));
  // Return to buttons page.
  $form_state['redirect'] = 'admin/config/socialshare/buttons/';
}

/**
 * Edit handler for button.
 */
function _socialshare_button_edit($form, &$form_state, $edit = array()) {
  $machine_name = arg(5);
  // Check if machine_name is available.
  if (empty($machine_name) || !socialshare_button_machine_name_load($machine_name)) {
    $message = t('This button is not available.');
    drupal_set_message($message, 'error');
    return FALSE;
  }

  $buttons_serialize = variable_get('socialshare_buttons', _socialshare_buttons_default());
  $buttons = unserialize($buttons_serialize);
  if (is_array($buttons) && count($buttons)) {
    $button = $buttons[$machine_name];
  }

  $form['button_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => $button->button_name,
      '#maxlength' => 255,
      '#required' => TRUE,
  );
  $form['button_machine_name'] = array(
      '#type' => 'machine_name',
      '#default_value' => $button->button_machine_name,
      '#maxlength' => 21,
      '#machine_name' => array(
          'exists' => 'socialshare_button_machine_name_load',
          'source' => array('button_name'),
      ),
  );
  $form['button_codes'] = array(
      '#title' => t('Button share codes'),
      '#type' => 'textarea',
      '#description' => t('Share codes such as url or javascript codes'),
      '#wysiwyg' => FALSE,
      '#required' => TRUE,
      '#default_value' => $button->button_codes,
  );
  $form['button_icon'] = array(
      '#title' => t('Upload button/icon'),
      '#type' => 'managed_file',
      '#description' => t('Allowed extensions: gif png jpg jpeg'),
      '#default_value' => $button->button_icon,
      '#required' => FALSE,
      '#upload_validators' => array(
          'file_validate_extensions' => array(
              'gif png jpg jpeg',
          ),
          // Pass the maximum file size in bytes.
          'file_validate_size' => array(1 * 1024 * 1024),
      ),
      '#upload_location' => 'public://socialshare/icons/',
  );
  $form['button_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enabled'),
      '#default_value' => $button->button_enabled,
  );
  
  $form['button_weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#delta' => 50,
      '#default_value' => $button->button_weight,
  );
  
  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
  );
  return $form;
}

/**
 * Edit button submit handler.
 */
function _socialshare_button_edit_submit($form, &$form_state) {
  $buttons_serialize = variable_get('socialshare_buttons', _socialshare_buttons_default());
  $buttons = unserialize($buttons_serialize);
  
  $buttons[$form_state['values']['button_machine_name']] = new stdClass();
  $buttons[$form_state['values']['button_machine_name']]->button_machine_name = $form_state['values']['button_machine_name'];
  $buttons[$form_state['values']['button_machine_name']]->button_name = $form_state['values']['button_name'];
  $buttons[$form_state['values']['button_machine_name']]->button_codes = $form_state['values']['button_codes'];
  $buttons[$form_state['values']['button_machine_name']]->button_icon = $form_state['values']['button_icon'];
  $buttons[$form_state['values']['button_machine_name']]->button_enabled = $form_state['values']['button_enabled'];
  $buttons[$form_state['values']['button_machine_name']]->button_weight = $form_state['values']['button_weight'];
  
  variable_set('socialshare_buttons', serialize($buttons));
  // Return to buttons page.
  $form_state['redirect'] = 'admin/config/socialshare/buttons/';  
}

/**
 * Delete button.
 */
function _socialshare_button_delete($form, &$form_state) {
  $machine_name = arg(5);
  // Check if machine_name is available.
  if (empty($machine_name) || !socialshare_button_machine_name_load($machine_name)) {
    $message = t('This button is not available.');
    drupal_set_message($message, 'error');
    return FALSE;
  }
  
  $buttons_serialize = variable_get('socialshare_buttons', _socialshare_buttons_default());
  $buttons = unserialize($buttons_serialize);
  if (is_array($buttons) && count($buttons)) {
    $button = $buttons[$machine_name];
  }
  
  $form['machine_name'] = array('#type' => 'value', '#value' => $button->button_machine_name);
  
  $form['#button'] = $button;
  $form['type'] = array('#type' => 'value', '#value' => 'button');
  $form['name'] = array('#type' => 'value', '#value' => $button->button_name);
  
  $form['delete'] = array('#type' => 'value', '#value' => TRUE);
  return confirm_form($form,
      t('Are you sure you want to delete the button %title?', array('%title' => $button->button_name)),
      'admin/config/socialshare/buttons',
      t('This action cannot be undone.'),
      t('Delete'),
      t('Cancel'));
}

/**
 * Delete button submit handler.
 */
function _socialshare_button_delete_submit($form, &$form_state) { 
  $machine_name = arg(5);
  if ($form_state['values']['machine_name']) {
    $buttons_serialize = variable_get('socialshare_buttons', _socialshare_buttons_default());
    $buttons = unserialize($buttons_serialize);
    if (is_array($buttons) && count($buttons)) {
      unset($buttons[$form_state['values']['machine_name']]);
      variable_set('socialshare_buttons', serialize($buttons));
    }
  }
  
  drupal_set_message(t('Deleted button %name.', array('%name' => $form_state['values']['name'])));
  $form_state['redirect'] = 'admin/config/socialshare/buttons';
  cache_clear_all();
  return;
}

/**
 * Callback to load existing machine name.
 */
function socialshare_button_machine_name_load($name) {
  $buttons_serialize = variable_get('socialshare_buttons', _socialshare_buttons_default());
  $buttons = unserialize($buttons_serialize);
  if (array_key_exists($name, $buttons)) {
    $button[] = $buttons[$name]->button_machine_name;
    return reset($button);
  } else {
    return FALSE;
  }
}
