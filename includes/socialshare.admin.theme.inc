<?php

/**
 * @file
 * admin theme.
 */

/**
 * Theme the share buttons admin page.
 *
 * @ingroup themeable
 */
function theme_socialshare_admin_buttons_list($variables = NULL) {
  $out = '';
  $out .= '<div>' . l(t('Add New Button'), 'admin/config/socialshare/buttons/add') . '</div>';
  $buttons_serialize = variable_get('socialshare_buttons', _socialshare_buttons_default());
  $buttons = unserialize($buttons_serialize);
  if (is_array($buttons) && count($buttons)) {
    usort($buttons, "_socialshare_cmp_up");
    foreach ($buttons as $value) {
      $row = array();
      $row[] = array(
        'data' => ($value->button_enabled ? t('Yes') : t('No')),
      );
      $row[] = array(
        'data' => $value->button_name,
      );
      $row[] = array(
        'data' => _socialshare_display_button($value->button_icon, $value->button_name),
      );
      $row[] = array(
        'data' => $value->button_weight,
      );
      $row[] = array(
        'data' => l(t('Edit'), 'admin/config/socialshare/buttons/edit/' . $value->button_machine_name) . ' | ' . l(t('Delete'), 'admin/config/socialshare/buttons/del/' . $value->button_machine_name),
      );
      $rows[] = $row;
    }
  }
  if (isset($rows) && count($rows)) {
    $header = array(
      t('Enabled'),
      t('Name'),
      t('Icon'),
      t('Weight'),
      t('Actions'),
    );
    $out .= theme('table', array(
      'header' => $header,
      'rows' => $rows,
    ));
  }
  else {
    $out .= '<b>' . t('No data') . '</b>';
  }
  return $out;
}
