<?php

/**
 * @file
 * Handler helper.
 */

/**
 * Rebuild icons.
 */
function _socialshare_rebuild() {
  $dir = socialshare_icon_dir();
  // Remove socialshare icon folder.
  file_unmanaged_delete_recursive($dir);
  // Create socialshare icon folder.
  file_prepare_directory($dir, FILE_CREATE_DIRECTORY);
  // Rebuild default icons.
  _socialshare_rebuild_icons();
  return TRUE;
}

/**
 * Copy icons from module.
 */
function _socialshare_rebuild_icons() {
  $dir = socialshare_icon_dir();
  $buttons = _socialshare_buttons_default();
  foreach ($buttons as $button_key => $button) {
    // Create a file object.
    $file = new stdClass();
    $file->uri = $button['button_icon_default'];
    $file->filemime = file_get_mimetype($button['button_icon_default']);
    $file->status = FILE_STATUS_PERMANENT;    
    $icon = file_copy($file, $dir, $replace = FILE_EXISTS_REPLACE);
    if (isset($icon->fid)) {
      $buttons[$button_key]['button_icon'] = $icon->fid;
      file_usage_add($icon, 'socialshare', 'SocialShareIcon', 0);
    } else {
      $buttons[$button_key]['button_icon'] = $button['button_icon_default'];
    }
  }
  variable_set('socialshare_buttons', serialize($buttons));
}

/**
 * Default icons.
 */
function _socialshare_buttons_default() {
  $buttons = array();
  $buttons['socialshare_facebook'] = array(
      'button_name' => 'Facebook',
      'button_machine_name' => 'socialshare_facebook',
      'button_codes' => 'http://www.facebook.com/sharer/sharer.php?u=[url:alias]',
      'button_icon' => '',
      'button_icon_default' => drupal_get_path('module', 'socialshare') . '/images/fb.png',
      'button_enabled' => TRUE,
      'button_weight' => 0,
  );
  $buttons['socialshare_twitter'] = array(
      'button_name' => 'Twitter',
      'button_machine_name' => 'socialshare_twitter',
      'button_codes' => 'http://twitter.com/intent/tweet?url=[url:alias]',
      'button_icon' => '',
      'button_icon_default' => drupal_get_path('module', 'socialshare') . '/images/tw.png',
      'button_enabled' => TRUE,
      'button_weight' => 1,
  );
  $buttons['socialshare_linkedin'] = array(
      'button_name' => 'LinkedIn',
      'button_machine_name' => 'socialshare_linkedin',
      'button_codes' => 'http://www.linkedin.com/shareArticle?mini=true&url=[url:alias]',
      'button_icon' => '',
      'button_icon_default' => drupal_get_path('module', 'socialshare') . '/images/in.png',
      'button_enabled' => TRUE,
      'button_weight' => 2,
  );
  $buttons['socialshare_gplus'] = array(
      'button_name' => 'Google Plus',
      'button_machine_name' => 'socialshare_gplus',
      'button_codes' => 'https://plus.google.com/share?url=[url:alias]',
      'button_icon' => '',
      'button_icon_default' => drupal_get_path('module', 'socialshare') . '/images/gplus.png',
      'button_enabled' => TRUE,
      'button_weight' => 3,
  );
  return $buttons;
}
